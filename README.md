# Evolution of the official PeerTube dark theme

Evolution of the official Dark theme for PeerTube.
Created by [Raph](https://tooter.social/@raph) with the precious help of [StefOfficiel](https://mastodon.stefofficiel.me/@stefofficiel).


## Screenshots

**Homepage**
![watch screen](./screens/home.png)

**Profile**
![watch screen](./screens/profile.png)

**Channel**
![watch screen](./screens/channel.png)

**Video**
![watch screen](./screens/video.png)

**Comments**
![watch screen](./screens/comments.png)
